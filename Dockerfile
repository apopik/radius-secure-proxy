From alpine:latest
run apk update
run apk add radsecproxy
run apk add gettext
run mkdir /runner
run chown 994:994 /runner
run chmod 700 /runner
ENV conf=/etc/radsecproxy.conf
EXPOSE 1812/udp
EXPOSE 1813/udp
EXPOSE 2083/tcp
COPY entrypoint.sh /entrypoint.sh
RUN chmod 755 /entrypoint.sh
ENTRYPOINT ["/entrypoint.sh"]
CMD ["/usr/sbin/radsecproxy", "-i", "/run/radsecproxy.pid", "-c", "/runner/radsecproxy.conf", "-f" ]
